import React, { FunctionComponent } from 'react'

import { Title } from '@/components/typography'

export const HelloWorld: FunctionComponent = () => {
  return <Title>Hello world</Title>
}
