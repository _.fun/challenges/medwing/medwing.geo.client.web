interface IConfig {
  uri: string
}

export const config: IConfig = {
  uri: process.env.URI,
}
