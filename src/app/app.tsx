import React, { FunctionComponent } from 'react'
import { Redirect, Route, Switch } from 'react-router-dom'

import { HelloWorld } from './helloWorld'
import { RouteTwo } from './routeTwo'

import { Error404 } from './error'

export const App: FunctionComponent = () => (
  <Switch>
    <Redirect path="/" exact to="/one" />
    <Route path="/one" component={HelloWorld} />
    <Route path="/two" component={RouteTwo} />
    <Route path="/*" component={Error404} status={404} />
  </Switch>
)
